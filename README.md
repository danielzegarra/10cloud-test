# 10cloud test

## Pre-requirements

 - sass (install instructions in http://sass-lang.com/)
 - npm

gulp and bower globally installed are recommended but not required.

## Installation

```shell
git clone https://dzegarra@bitbucket.org/dzegarra/10cloud-test.git
cd 10cloud-test
npm install
npm run bower install
```

## Build

Compiled version is saved in `/dist` directory.

```shell
gulp build
```
or
```shell
npm run build
```
The second version is only useful if `gulp-cli` is not globally installed. 

## Serve compiled version

```shell
gulp serve:dist
```
or
```shell
npm run serve:dist
```

## Run unit tests

```shell
gulp test
```
or
```shell
npm run test
```

## Screenshots

### Desktop
![desktop.gif](https://bitbucket.org/repo/7qbkxb/images/3827306158-desktop.gif)

### Mobile
![rwd.gif](https://bitbucket.org/repo/7qbkxb/images/1592967920-rwd.gif)