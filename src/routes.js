(function (module) {

    /** @ngInject */
    module.config(function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('login', {
                url: '/',
                templateUrl: 'app/partials/login.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            })
            .state('register', {
                url: '/register',
                templateUrl: 'app/partials/register.html',
                controller: 'RegisterController',
                controllerAs: 'vm'
            })
            .state('report', {
                url: '/report',
                templateUrl: 'app/partials/report.html',
                controller: 'ReportController',
                controllerAs: 'vm'
            });
    });

    // Don't allow to a unauthenticated user to access other states rather than the login screen
    /** @ngInject */
    module.run(function ($rootScope, session, $state) {
        // eslint-disable-next-line
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            if (!session.isLogged() && toState.name !== 'login') {
                event.preventDefault();// You shall not pass!
                $state.go('login');
            }
        });
    });

})(angular.module('dzegarra.10cloud-test'));
