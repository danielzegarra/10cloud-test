module.exports = {
  extends: [
    'angular'
  ],
  rules: {
    'angular/no-service-method': 0,
    'indent': ["error", 4],
    'padded-blocks': "off",
    'no-trailing-spaces':  ["error", {
      'skipBlankLines': true
    }],
    'one-var': ["error", "always"],
    'angular/module-getter': 'off'
  }
};