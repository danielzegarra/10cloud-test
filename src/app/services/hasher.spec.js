describe('hasher service', function () {
    var hasher,
        md5,
        txt = 'abracadabra';

    beforeEach(module('dzegarra.10cloud-test'));
    beforeEach(angular.mock.inject(function (_hasher_, _md5_) {
        hasher = _hasher_;
        md5 = _md5_;
    }));

    // Dummy test I know, but there is nothing more complicated to test
    it('should generate a hash based in a input string', function () {
        expect(hasher(txt)).toEqual(md5.createHash(txt).substr(0, 8));
    });

});
