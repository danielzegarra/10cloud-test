(function (module) {

    /**
     * @ngdoc service
     * @name log
     * @module dzegarra.10cloud-test
     * @description Store the items data and manage its contents
     */
    /* @ngInject */
    module.factory('log', function ($localStorage, $mdToast, session, hasher) {
        var storage = $localStorage.$default({
            rows: []
        });

        function showToast(value) {
            var msg = value ? ('Value "' + value + '" added') : 'Empty value added';
            $mdToast.show($mdToast.simple().textContent(msg).position('bottom right'));
        }

        function addToStack(value) {
            storage.rows.push({
                userEmail: session.getEmail(),
                input: value,
                hash: hasher(value),
                creationDate: new Date()
            });
        }

        /**
         * Be aware that this fn returns TRUE only if the item is added.
         * @param {string} value
         * @returns {boolean}
         */
        function addItem(value) {
            value = value || '';
            if (session.isLogged()) {
                addToStack(value);
                showToast(value);
                return true;
            }
            return false;
        }

        return {
            rows: storage.rows,
            addItem: addItem
        };
    });

})(angular.module('dzegarra.10cloud-test'));

