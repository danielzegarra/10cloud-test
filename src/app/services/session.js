(function (module) {

    /**
     * @ngdoc service
     * @name session
     * @module dzegarra.10cloud-test
     * @description Manage the sessions
     */
    /* @ngInject */
    module.factory('session', function ($sessionStorage) {
        var storage = $sessionStorage.$default({
            email: ''
        });
        return {
            getEmail: function () {
                return this.isLogged() ? storage.email : null;
            },
            isLogged: function () {
                return Boolean(storage.email);
            },
            login: function (email) {
                storage.email = email;
                return true;
            },
            logout: function () {
                storage.email = '';
            }
        };
    });

})(angular.module('dzegarra.10cloud-test'));

