(function (module) {

    /**
     * @ngdoc service
     * @name hasher
     * @module dzegarra.10cloud-test
     * @description Simply generate a 8 characters hash based on the string given
     */
    module.factory('hasher', function (md5) {
        return function (input) {
            return md5.createHash(input || '').substr(0, 8);
        };
    });

})(angular.module('dzegarra.10cloud-test'));
