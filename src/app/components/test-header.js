(function (module) {

    /**
     * @ngdoc component
     * @name testHeader
     * @module dzegarra.10cloud-test
     * @description Main header of the app
     */
    module.component('testHeader', {
        templateUrl: 'app/components/test-header.html',
        controller: TestHeaderController,
        controllerAs: 'vm'
    });

    /* @ngInject */
    function TestHeaderController($state, session) {
        var vm = this;
        vm.session = session;
        vm.logout = function () {
            session.logout();
            $state.go('login');
        };
    }

})(angular.module('dzegarra.10cloud-test'));
