(function (module) {

    /**
     * @ngdoc controller
     * @name RegisterController
     * @module dzegarra.10cloud-test
     * @description Contains a form to add new items
     */
    module.controller('RegisterController', RegisterController);

    /** @ngInject */
    function RegisterController($state, log) {
        var vm = this;
        vm.save = function (inputValue) {
            if (log.addItem(inputValue)) {
                vm.goBack();
            }
        };
        vm.goBack = function () {
            $state.go('report');
        };
    }

})(angular.module('dzegarra.10cloud-test'));

