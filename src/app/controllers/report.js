(function (module) {

    /**
     * @ngdoc controller
     * @name ReportController
     * @module dzegarra.10cloud-test
     * @description Shows a nice report of the items in localstorage
     */
    module.controller('ReportController', ReportController);

    /** @ngInject */
    function ReportController($state, log) {
        var vm = this;
        vm.rows = log.rows;
        vm.currentSort = 'input';
        vm.requestAddItem = function () {
            $state.go('register');
        };
        vm.hasItems = function () {
            return vm.rows.length > 0;
        };
    }

})(angular.module('dzegarra.10cloud-test'));
