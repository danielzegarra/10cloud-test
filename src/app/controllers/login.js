(function (module) {

    /**
     * @ngdoc controller
     * @name LoginController
     * @module dzegarra.10cloud-test
     * @description Login form
     */
    module.controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($state, session) {
        var vm = this;
        vm.login = function (email) {
            if (session.login(email)) {
                $state.go('report');
            }
        };
    }

})(angular.module('dzegarra.10cloud-test'));
