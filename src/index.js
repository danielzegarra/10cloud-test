(function (angular) {

    angular.module('dzegarra.10cloud-test', [
        'ui.router',
        'ngMaterial',
        'md.data.table',
        'ngStorage',
        'angular-md5'
    ]);

})(angular);
